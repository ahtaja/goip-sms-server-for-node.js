# GoIP SMS Server for Node.js

This is a demo to show how to connect GoIP GSM gateway to node.js application and receive messages from it.
Only thing this does, is echo the received SMS messages from GoIP gateway to the screen.

# What is needed?

You need:

1. A GoIP GSM gateway which can be purchased from ebay, amazon, aliexpress, etc. about 60 euros. 
2. Computer which is capable of running node.js 

# how to do this?

1. Configure your GoIP GSM gateway to connect your computer (ip-addess and port)

![GoIP SMS settings](manuals/GoIp%20sms%20settings.png)

2. run enclosed server code.

# more info
https://www.voip-info.org/goip/
