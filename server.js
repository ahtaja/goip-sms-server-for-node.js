// UDP Sample Server

var s_port = 44444;
var dgram = require("dgram");
var server = dgram.createSocket("udp4");

server.bind(s_port);

server.on("listening", function() {
  var address = server.address();
  console.log("server listening " + address.address + ":" + address.port);
});

server.on("message", function(msg, rinfo) {
  console.log("server got a message from " + rinfo.address + ":" + rinfo.port);

  let str = msg.toString('utf8');
  var result = {};

  str.split(';').forEach(function(x){
    var arr = x.split(':');
    arr[1] && (result[arr[0]] = arr[1]);
  });
  console.log(result);

  var ack = new Buffer("ack");
  server.send(ack, 0, ack.length, rinfo.port, rinfo.address, function(err, bytes) {
    console.log("sent ACK.");
  }); 
});

server.on("error", function(err) {
  console.log("server error: \n" + err.stack);
  server.close();
});

server.on("close", function() {
  console.log("closed.");
});

